Here you can find a demo of a C++ application with an automated testing using Bitbucket Pipelines service.

Anytime you want to start a new project in C++, feel free to use this code as a skeleton.
Copy the contents to your repository and build your code upon it.

File demo.cpp contains a code for fibonacci() function.

File demo_test.cpp has a few tests to make sure the fibonacci() function works correctly.

To test the code from command line, run:

    make test

After every commit, visit the Pipelines tab and review the results of automated testing.

Links:

 * https://github.com/google/googletest/blob/master/googletest/docs/primer.md
 * https://support.atlassian.com/bitbucket-cloud/docs/get-started-with-bitbucket-pipelines/

Source files:

 * demo.hpp - declaration of fibonacci() routine
 * demo.cpp - implementation of fibonacci() routine
 * demo_test.cpp - tests for fibonacci() routine, using Googletest
 * gtest/gtest.h - header file of Googletest framework
 * gtest/gtest-all.cpp - fused sources of Googletest framework, created by script https://github.com/google/googletest/blob/master/googletest/scripts/fuse_gtest_files.py
